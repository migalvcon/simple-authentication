# Prerequisites

The project is based on Scala, so the next technologies have to be installed.

* Java 8
* SBT 1.0.3

# Run

Execute `sbt run` and the server will start in the port 8080. Press **Enter** for stopping it.

# Libraries

According with the recommendations the project uses the next:

* Akka Actors
* Akka Http

In addition, it uses **Spray** for mapping JSON entities and **Specs2** for testing.

# API

The project contains a Postman collection with an example of each request.

# Development decisions

Given the recommendation to use the Actor Model implementation and taking into account that it is the
first time I used Akka, there were a few decisions I took during the development of the task after a
lot of research.

1. I went for a traditional structure based on separation of concepts, so the project is separated
in the next:

    * **actors**: the actors based on _Akka Actors_.
    * **models**: the models described in the task.
    * **routes**: the routes that are served by the server.
    * **services**: the services that offer the functionality of the system.
    * **utils**: the tools/helpers.

2. There are two routes and I separated them in two Scala classes because the testing is more easy
and clean:

    * **HealthRoutes**: healthy endpoints.
    * **AuthRoutes**: authentication endpoints.

3. I developed 3 actors based on each functionality:

    * **CredentialsActor**:  authentication.
    * **TokenActor**: token generation.
    * **AuthRequestActor**: composition and orchestration.

4. The authentication request is managed by **AuthRequestActor** who is responsible for the orchestration
of the authentication and the token generator functionalities. Therefore, **AuthRequestActor** implements
the **SimpleAsynTokenService** service.

   I also thought in implements a service for **SimpleAsynTokenService** and it would use
**AuthRequestActor**, but for simplicity I went for the before approach.