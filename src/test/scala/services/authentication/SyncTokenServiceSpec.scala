package services.authentication

import models.{Credentials, User, UserToken}
import org.specs2.mutable.SpecificationLike

class SyncTokenServiceSpec extends SpecificationLike {
  "SyncTokenService" should {
    "return a token when authentication and token do not fail" in {
      val syncTokenService: SyncTokenService = new SyncTokenService {
        override protected def authenticate(credentials: Credentials): User = User("username")

        override protected def issueToken(user: User): UserToken = UserToken("token")
      }

      syncTokenService.requestToken(Credentials("username", "password")).token must beEqualTo("token")
    }

    "return a failure when authentication fails" in {
      val syncTokenService: SyncTokenService = new SyncTokenService {
        override protected def authenticate(credentials: Credentials): User = throw new Exception()

        override protected def issueToken(user: User): UserToken = UserToken("token")
      }

      syncTokenService.requestToken(Credentials("username", "password")) must throwAn[Exception]
    }

    "return a failure when token fails" in {
      val syncTokenService: SyncTokenService = new SyncTokenService {
        override protected def authenticate(credentials: Credentials): User = User("username")

        override protected def issueToken(user: User): UserToken = throw new Exception()
      }

      syncTokenService.requestToken(Credentials("username", "password")) must throwAn[Exception]
    }
  }
}
