package services.authentication

import models.{Credentials, User, UserToken}
import org.specs2.mutable.SpecificationLike

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class AsyncTokenServiceSpec extends SpecificationLike {
  "AsyncTokenService" should {
    "return a token when authentication and token do not fail" in {
      val asyncTokenService: AsyncTokenService = new AsyncTokenService {
        override protected def authenticate(credentials: Credentials): Future[User] = Future.successful(User("username"))

        override protected def issueToken(user: User): Future[UserToken] = Future.successful(UserToken("token"))
      }

      Await.result(asyncTokenService.requestToken(Credentials("username", "password")), Duration.Inf).token must beEqualTo("token")
    }

    "return a failure when authentication fails" in {
      val asyncTokenService: AsyncTokenService = new AsyncTokenService {
        override protected def authenticate(credentials: Credentials): Future[User] = Future.failed(new Exception())

        override protected def issueToken(user: User): Future[UserToken] = Future.successful(UserToken("token"))
      }

      Await.result(asyncTokenService.requestToken(Credentials("username", "password")), Duration.Inf) must throwAn[Exception]
    }

    "return a failure when token fails" in {
      val asyncTokenService: AsyncTokenService = new AsyncTokenService {
        override protected def authenticate(credentials: Credentials): Future[User] = Future.successful(User("username"))

        override protected def issueToken(user: User): Future[UserToken] = Future.failed(new Exception())
      }

      Await.result(asyncTokenService.requestToken(Credentials("username", "password")), Duration.Inf) must throwAn[Exception]
    }
  }
}
