package actors

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import akka.util.Timeout
import models.{Credentials, User}
import org.specs2.mutable.SpecificationLike

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class CredentialsActorSpec extends TestKit(ActorSystem()) with ImplicitSender with SpecificationLike {
  implicit val timeout: Timeout = Timeout(30.seconds)

  "CredentialsActor" should {
    val credentialsActorRef: TestActorRef[CredentialsActor] = TestActorRef(CredentialsActor.props())

    "send back an User when Credentials password is equal to username in uppercase" in {
      val result: Future[User] = (credentialsActorRef ? Credentials("username", "USERNAME")).mapTo[User]

      Await.result(result, Duration.Inf).userId must beEqualTo("username")
    }

    "send back failure when Credentials password is not equal to username in uppercase" in {
      val result: Future[User] = (credentialsActorRef ? Credentials("username", "username")).mapTo[User]

      Await.result(result, Duration.Inf) must throwAn[Exception]
    }

    "send back failure when unrecognized message" in {
      val result: Future[User] = (credentialsActorRef ? "unrecognized message").mapTo[User]

      Await.result(result, Duration.Inf) must throwAn[Exception]
    }
  }
}