package actors

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import akka.util.Timeout
import models.{User, UserToken}
import org.specs2.mutable.SpecificationLike

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class TokenActorSpec extends TestKit(ActorSystem()) with ImplicitSender with SpecificationLike {
  implicit val timeout: Timeout = Timeout(30.seconds)

  "TokenActor" should {
    val tokenActorRef: TestActorRef[CredentialsActor] = TestActorRef(TokenActor.props())

    "send back an UserToken when User userid does not start with 'A'" in {
      val result: Future[UserToken] = (tokenActorRef ? User("username")).mapTo[UserToken]

      Await.result(result, Duration.Inf).token must beMatching("username_\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}")
    }

    "send back failure when User userid starts with 'A'" in {
      val result: Future[UserToken] = (tokenActorRef ? User("Abcde")).mapTo[UserToken]

      concurrent.Await.result(result, Duration.Inf) must throwAn[Exception]
    }

    "send back failure when unrecognized message" in {
      val result: Future[UserToken] = (tokenActorRef ? "unrecognized message").mapTo[UserToken]

      Await.result(result, Duration.Inf) must throwAn[Exception]
    }
  }
}