package actors

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import akka.util.Timeout
import models.{Credentials, UserToken}
import org.specs2.mutable.SpecificationLike

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class AuthRequestActorSpec extends TestKit(ActorSystem()) with ImplicitSender with SpecificationLike {
  implicit val timeout: Timeout = Timeout(30.seconds)

  "AuthRequestActor" should {
    val authRequestActorRef: TestActorRef[AuthRequestActor] = TestActorRef(AuthRequestActor.props())

    "send back failure when credentials are invalid" in {
      val result: Future[UserToken] = (authRequestActorRef ? Credentials("wrong", "credentials")).mapTo[UserToken]

      Await.result(result, Duration.Inf) must throwAn[Exception]
    }

    "send back failure when username is invalid" in {
      val result: Future[UserToken] = (authRequestActorRef ? Credentials("Abcde", "ABCDE")).mapTo[UserToken]

      Await.result(result, Duration.Inf) must throwAn[Exception]
    }

    "send back a token when credentials are valid" in {
      val result: Future[UserToken] = (authRequestActorRef ? Credentials("username", "USERNAME")).mapTo[UserToken]

      Await.result(result, Duration.Inf).token must beMatching("username_\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}")
    }

    "send back failure when unrecognized message" in {
      val result: Future[UserToken] = (authRequestActorRef ? "unrecognized message").mapTo[UserToken]

      Await.result(result, Duration.Inf) must throwAn[Exception]
    }
  }
}
