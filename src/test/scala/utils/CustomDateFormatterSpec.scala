package utils

import org.specs2.mutable.SpecificationLike

class CustomDateFormatterSpec extends SpecificationLike {
  "CustomDateFormatter" should {
    "return the current time in long format" in {
      CustomDateFormatter.getCurrentTimeLongStringFormat() must beMatching("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}")
    }
  }
}
