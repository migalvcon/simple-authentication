package routes

import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.Specs2RouteTest
import org.specs2.mutable.SpecificationLike

class HealthRoutesSpec extends Specs2RouteTest with SpecificationLike {
  "The service" should {
    val routes: Route = HealthRoutes.healthRoutes()

    "return a HTML entity for GET requests to the health path" in {
      Get("/health") ~> routes ~> check {
        responseEntity.getContentType() shouldEqual ContentTypes.`text/html(UTF-8)`
      }
    }

    "return a message for GET requests to the health path" in {
      Get("/health") ~> routes ~> check {
        responseAs[String] shouldEqual "<h1>Service running!</h1>"
      }
    }
  }
}
