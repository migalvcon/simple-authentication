package routes

import actors.AuthRequestActor
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.{RouteTestTimeout, Specs2RouteTest}
import akka.testkit.TestActorRef
import akka.testkit.TestDuration
import models.UserToken
import org.specs2.mutable.SpecificationLike
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

import scala.concurrent.duration._

class AuthRoutesSpec extends Specs2RouteTest with SpecificationLike {
  implicit val routeTestTimeout: RouteTestTimeout = RouteTestTimeout(60.seconds dilated)
  implicit val userTokenFormat: RootJsonFormat[UserToken] = jsonFormat1(UserToken)

  "The service" should {
    val actorRef: TestActorRef[AuthRequestActor] = TestActorRef(AuthRequestActor.props())
    val routes: Route = AuthRoutes.authRoutes(actorRef)

    "return a success for POST requests to the auth path when credentials are correct" in {
      val credentials: String =
        """{
          | "username": "username",
          | "password": "USERNAME"
          |}""".stripMargin

      Post("/auth", HttpEntity(ContentTypes.`application/json`, string = credentials)) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseEntity.getContentType() shouldEqual ContentTypes.`application/json`
        responseAs[UserToken].token must beMatching("username_\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}")
      }
    }

    "return a failure for POST requests to the auth path when credentials are incorrect" in {
      val credentials: String =
        """{
          | "username": "username",
          | "password": "password"
          |}""".stripMargin

      Post("/auth", HttpEntity(ContentTypes.`application/json`, string = credentials)) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseEntity.getContentType() shouldEqual ContentTypes.`text/plain(UTF-8)`
        responseAs[String] shouldEqual "Invalid credentials"
      }
    }

    "return a failure for POST requests to the auth path when user is invalid" in {
      val credentials: String =
        """{
          | "username": "Abcde",
          | "password": "ABCDE"
          |}""".stripMargin

      Post("/auth", HttpEntity(ContentTypes.`application/json`, string = credentials)) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
        responseEntity.getContentType() shouldEqual ContentTypes.`text/plain(UTF-8)`
        responseAs[String] shouldEqual "Invalid user"
      }
    }
  }
}
