package utils

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

object CustomDateFormatter {
  def getCurrentTimeLongStringFormat(): String = {
    val now: Date = Calendar.getInstance().getTime()
    val formatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")

    formatter.format(now)
  }
}
