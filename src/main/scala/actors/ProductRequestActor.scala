package actors

import akka.actor.Status.Failure
import akka.actor.{Actor, ActorLogging, ActorRef, Props, Scheduler}
import models.{AddOp, GetOp, Product, ResetOp}
import services.product.ProductService

import scala.util.Random
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class ProductRequestActor extends ProductService with Actor with ActorLogging {
  private val scheduler: Scheduler = context.system.scheduler

  private var products: Seq[Product] = Nil

  override def receive: Receive = {
    case _: ResetOp =>
      val senderRef: ActorRef = sender()

      scheduler.scheduleOnce(Random.nextInt(5000).milliseconds) {
        log.info("Operation: ResetOp")

        reset()

        senderRef ! ""
      }
    case _: GetOp =>
      val senderRef: ActorRef = sender()

      scheduler.scheduleOnce(Random.nextInt(5000).milliseconds) {
        log.info("Operation: GetOp")

        senderRef ! getProducts()
      }
    case operation: AddOp => {
      val senderRef: ActorRef = sender()

      scheduler.scheduleOnce(Random.nextInt(5000).milliseconds) {
        val product: Product = operation.product

        log.info(s"Operation: AddOp - ${product}")

        addProduct(product)

        senderRef ! product
      }
    }
    case message => {
      log.error(s"Unrecognized message: ${message}")

      sender() ! Failure(new Exception("Invalid message type"))
    }
  }

  override def reset(): Unit =
    products = Nil

  override def getProducts(): Seq[Product] =
    products

  override def addProduct(product: Product): Product = {
    products = products :+ product

    product
  }
}

object ProductRequestActor {
  def props(): Props =
    Props(new ProductRequestActor())
}