package actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Scheduler}
import akka.actor.Status.Failure
import models.{Credentials, User}

import scala.util.Random
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class CredentialsActor extends Actor with ActorLogging {
  private val scheduler: Scheduler = context.system.scheduler

  override def receive: Receive = {
    case credentials: Credentials =>
      val senderRef: ActorRef = sender()

      scheduler.scheduleOnce(Random.nextInt(5000).milliseconds) {
        val username: String = credentials.username
        val password: String = credentials.password

        log.info(s"Credentials: ${username} - ${password}")

        if (!username.toUpperCase().equals(password)) {
          log.error("Invalid credentials")

          senderRef ! Failure(new Exception("Invalid credentials"))
        } else {
          senderRef ! User(credentials.username)
        }
      }
    case message => {
      log.error(s"Unrecognized message: ${message}")

      sender() ! Failure(new Exception("Invalid message type"))
    }
  }
}

object CredentialsActor {
  def props(): Props =
    Props(new CredentialsActor())
}