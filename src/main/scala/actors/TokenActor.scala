package actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Scheduler}
import akka.actor.Status.Failure
import models.{User, UserToken}
import utils.CustomDateFormatter

import scala.util.Random
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class TokenActor extends Actor with ActorLogging {
  private val scheduler: Scheduler = context.system.scheduler

  override def receive: Receive = {
    case user: User =>
      val senderRef: ActorRef = sender()

      scheduler.scheduleOnce(Random.nextInt(5000).milliseconds) {
        log.info(s"UserId: ${user.userId}")

        if (user.userId.startsWith("A")) {
          log.error("Invalid userId")

          senderRef ! Failure(new Exception("Invalid user"))
        } else {
          val now: String = CustomDateFormatter.getCurrentTimeLongStringFormat()

          senderRef ! UserToken(s"${user.userId}_${now}")
        }
      }
    case message => {
      log.error(s"Unrecognized message: ${message}")

      sender() ! Failure(new Exception("Invalid message type"))
    }
  }
}

object TokenActor {
  def props(): Props =
    Props(new TokenActor())
}