package actors

import akka.actor.Status.Failure
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import models.{Credentials, User, UserToken}
import services.authentication.SimpleAsyncTokenService

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}

class AuthRequestActor extends SimpleAsyncTokenService with Actor with ActorLogging {
  implicit private val timeout: Timeout = Timeout(10.seconds)
  implicit private val executionContext: ExecutionContextExecutor = context.dispatcher

  private val credentialsActor: ActorRef = context.actorOf(CredentialsActor.props())
  private val tokenActor: ActorRef = context.actorOf(TokenActor.props())

  override def receive: Receive = {
    case credentials: Credentials => {
      val senderRef: ActorRef = sender()

      requestToken(credentials).foreach(userToken => senderRef ! userToken)
    }
    case message => {
      log.error(s"Unrecognized message: ${message}")

      sender() ! Failure(new Exception("Invalid message type"))
    }
  }

  override def requestToken(credentials: Credentials): Future[UserToken] = {
    for {
      user <- (credentialsActor ? credentials).mapTo[User]
      token <- (tokenActor ? user).mapTo[UserToken]
    } yield token
  }
}

object AuthRequestActor {
  def props(): Props = Props(new AuthRequestActor())
}