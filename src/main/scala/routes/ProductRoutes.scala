package routes

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives.{as, complete, entity, get, onComplete, path, post}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import models.{AddOp, GetOp, Product, ResetOp}
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object ProductRoutes {
  implicit val timeout: Timeout = Timeout(10.seconds)

  implicit val productFormat: RootJsonFormat[Product] = jsonFormat4(Product)

  def productRoutes(productRequestActor: ActorRef): Route =
    get {
      path("products" / "reset") {
        val result: Future[String] = (productRequestActor ? ResetOp()).mapTo[String]

        onComplete(result) {
          case Success(_) => complete(StatusCodes.NoContent)
          case Failure(exception) => complete(StatusCodes.BadRequest, exception.getMessage)
        }
      }
    } ~
    get {
      path("products") {
        val result: Future[Seq[Product]] = (productRequestActor ? GetOp()).mapTo[Seq[Product]]

        onComplete(result) {
          case Success(products) => complete(products)
          case Failure(exception) => complete(StatusCodes.BadRequest, exception.getMessage)
        }
      }
    } ~
    post {
      path("products") {
        entity(as[Product]) { product: Product =>
          val result: Future[Product] = (productRequestActor ? AddOp(product)).mapTo[Product]

          onComplete(result) {
            case Success(product) => complete(product)
            case Failure(exception) => complete(StatusCodes.BadRequest, exception.getMessage)
          }
        }
      }
    }
}
