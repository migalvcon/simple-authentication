package routes

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives.{as, complete, entity, onComplete, path, post}
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import models.{Credentials, UserToken}
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object AuthRoutes {
  implicit val timeout: Timeout = Timeout(10.seconds)

  implicit val credentialsFormat: RootJsonFormat[Credentials] = jsonFormat2(Credentials)
  implicit val userTokenFormat: RootJsonFormat[UserToken] = jsonFormat1(UserToken)

  def authRoutes(authRequestActor: ActorRef): Route =
    post {
      path("auth") {
        entity(as[Credentials]) { credentials: Credentials =>
          val result: Future[UserToken] = (authRequestActor ? credentials).mapTo[UserToken]

          onComplete(result) {
            case Success(userToken) => complete(userToken)
            case Failure(exception) => complete(StatusCodes.BadRequest, exception.getMessage)
          }
        }
      }
    }
}
