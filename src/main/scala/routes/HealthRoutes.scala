package routes

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives.{complete, get, path}

object HealthRoutes {
  def healthRoutes(): Route =
    get {
      path("health") {
        complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Service running!</h1>"))
      }
    }
}
