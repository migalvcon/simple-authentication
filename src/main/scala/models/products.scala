package models

case class Product(id: Long, name: String, brand: String, price: Double)

trait ProductOperation
case class GetOp() extends ProductOperation
case class AddOp(product: Product) extends ProductOperation
case class ResetOp() extends ProductOperation