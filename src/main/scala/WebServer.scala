package web

import actors.{AuthRequestActor, ProductRequestActor}
import akka.actor.{ActorRef, ActorSystem, PoisonPill}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import routes.{AuthRoutes, HealthRoutes, ProductRoutes}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.io.StdIn

object WebServer extends App {
  implicit private val actorSystem: ActorSystem = ActorSystem()
  implicit private val actorMaterializer: ActorMaterializer = ActorMaterializer()
  implicit private val executionContext: ExecutionContextExecutor = actorSystem.dispatcher

  private val authRequestActor: ActorRef = actorSystem.actorOf(AuthRequestActor.props())
  private val productRequestActor: ActorRef = actorSystem.actorOf(ProductRequestActor.props())

  private val routes: Route =
    HealthRoutes.healthRoutes() ~
    AuthRoutes.authRoutes(authRequestActor) ~
    ProductRoutes.productRoutes(productRequestActor)

  private val server: Future[Http.ServerBinding] = Http().bindAndHandle(routes, "localhost", 8080)

  println("Server online at http://localhost:8080/")
  println("Press RETURN to stop...")

  StdIn.readLine()

  productRequestActor ! PoisonPill
  authRequestActor ! PoisonPill

  actorSystem.terminate()

  server
    .flatMap(_.unbind())
    .onComplete(_ => actorSystem.terminate())
}
