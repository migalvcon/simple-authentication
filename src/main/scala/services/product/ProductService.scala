package services.product

import models.Product

trait ProductService {
  def reset(): Unit
  def getProducts(): Seq[Product]
  def addProduct(product: Product): Product
}
