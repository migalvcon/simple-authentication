lazy val commonSettings = Seq(
  version := "0.1",
  scalaVersion := "2.12.4",

  testOptions in Test += Tests.Argument("showtimes"),

  cancelable in Global := true,
  connectInput in run := true,

  fork in run := true
)

val akkaHttpVersion = "10.0.10"
val sprayJsonVersion = "1.3.4"
val specs2Version = "4.0.0"

lazy val root = (project in file("."))
  .settings(
    name := "Simple Authentication",
    commonSettings,
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "io.spray"          %% "spray-json"           % sprayJsonVersion,
      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion   % Test,
      "org.specs2"        %% "specs2-core"          % specs2Version     % Test
    )
  )